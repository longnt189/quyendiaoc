module.exports = (currentPage, perPage) => {
	return (currentPage -1) * perPage + 1;
};
