export default (text, maxLength = 50) => {
	if (text.length > maxLength) text = `${text.substr(0, maxLength)}...`;

	return text;
};
