const express = require('express');
const router = express.Router();

const fs = require('fs');

const { signUp,
	changePassword,
	getSubscribers,
	handleSubscribe,
	deleteSubscribe
} = require('../controllers');
require('../services/passport.config');

router.use('/', (req, res, next) => {
	if (req.isAuthenticated()) return next();

	return res.redirect('/auth');
});

router.get('/', (req, res) => {
	res.redirect('/dashboard');
});

router.get('/dashboard', (req, res) => {
	res.render('admin/main/dashboard');
});

router.get('/profile', (req, res) => {
	res.render('admin/main/profile');
});

router.post('/profile/change-password', (req, res, next) => {
	changePassword(req, res, next);
});

router.route('/create-account')
	.get((req, res) => {
		res.render('admin/main/signUp');
	})
	.post((req, res, next) => {
		signUp(req, res, next);
	});

router.get('/subscribers', (req, res, next) => {
	getSubscribers(req, res, next);
});

router.post('/handle-subscribe', (req, res, next) => {
	handleSubscribe(req, res, next);
});

router.post('/delete-subscribe', (req, res, next) => {
	deleteSubscribe(req, res, next);
});

router.get('/robots', (req, res, next) => {
	fs.readFile(process.cwd() + '/robots.txt', 'utf8', (err, content) => {
		if (err) return next(err);
		res.render('admin/main/robots_editor', { content });
	});
});

router.post('/robots/edit', (req, res, next) => {
	fs.writeFile(process.cwd() + '/robots.txt', req.body.robots, 'utf8', err => {
		if (err) return next(err);
		req.flash('success', 'Cập nhật file Robots.txt thành công!');
		res.redirect('/admin/robots');
	});
});

module.exports = router;
