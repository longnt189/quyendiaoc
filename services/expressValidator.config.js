module.exports = {
    signUpValidatorConfig: (req) => {
		req.checkBody({
			name: {
				isLength: {
					options: [{ min: 1 }],
					errorMessage: 'Bạn chưa nhập Họ tên'
				}
			},
			email: {
				isLength: {
					options: [{ min: 1 }],
					errorMessage: 'Bạn chưa nhập Email'
				},
				isEmail: {
					errorMessage: 'Email không hợp lệ'
				}
			},
			password: {
				isLength: {
					options: [{ min: 6 }],
					errorMessage: 'Mật khẩu phải trên 6 ký tự'
				}
			}
		});

		req.checkBody('repassword', 'Mật khẩu không khớp').equals(req.body.password);

		return req.validationErrors();
	}
};
