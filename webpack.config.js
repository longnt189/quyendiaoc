const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	entry: ["./src/scss/main.scss", "./src/js/main.js"],
	output: {
		path: __dirname + "/public/client",
		filename: "js/main.bundle.js"
	},
	target: "web",
	module: {
		rules: [
			{
				test: /\.js$/,
				use: "babel-loader",
				exclude: /node_modules/
			},
			{
				test: /\.(scss|css)$/,
				use: ["css-loader"].concat(
					ExtractTextPlugin.extract({
						fallback: "style-loader",
						use: [
							{
								loader: "css-loader",
								options: { minimize: true }
							},
							"sass-loader"
						]
					})
				)
			},
			{
				test: /\.(png|jpg)$/,
				use: [
					"file-loader?name=[name].[ext]&outputPath=imgs/&publicPath=../",
					"image-webpack-loader"
				]
			},
			{
				test: /\.(svg|woff|woff2|eot|otf|TTF|ttf)$/,
				use: [
					"file-loader?name=[name].[ext]&outputPath=fonts/&publicPath=../"
				]
			}
		]
	},
	plugins: [new ExtractTextPlugin("css/style.bundle.css")]
};
