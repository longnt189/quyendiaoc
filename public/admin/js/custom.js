(function($,sr){
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    const debounce = function (func, threshold, execAsap) {
        let timeout;

        return function debounced () {
            const obj = this, args = arguments;
            function delayed () {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            }

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    };

    // smartresize
    jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

const CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $MENU_TOGGLE = $('#menu_toggle'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');

// Sidebar
function init_sidebar() {
// TODO: This is some kind of easy fix, maybe we can improve this
    var setContentHeight = function () {
        // reset height
        $RIGHT_COL.css('min-height', $(window).height());

        var bodyHeight = $BODY.outerHeight(),
            footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
            leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

        // normalize content
        contentHeight -= $NAV_MENU.height() + footerHeight;

        $RIGHT_COL.css('min-height', contentHeight);
    };

    $SIDEBAR_MENU.find('a').on('click', function(ev) {
        var $li = $(this).parent();

        if ($li.is('.active')) {
            $li.removeClass('active active-sm');
            $('ul:first', $li).slideUp(function() {
                setContentHeight();
            });
        } else {
            // prevent closing menu if we are on child menu
            if (!$li.parent().is('.child_menu')) {
                $SIDEBAR_MENU.find('li').removeClass('active active-sm');
                $SIDEBAR_MENU.find('li ul').slideUp();
            }else
            {
                if ( $BODY.is( ".nav-sm" ) )
                {
                    $SIDEBAR_MENU.find( "li" ).removeClass( "active active-sm" );
                    $SIDEBAR_MENU.find( "li ul" ).slideUp();
                }
            }
            $li.addClass('active');

            $('ul:first', $li).slideDown(function() {
                setContentHeight();
            });
        }
    });

// toggle small or large menu
    $MENU_TOGGLE.on('click', function() {

        if ($BODY.hasClass('nav-md')) {
            $SIDEBAR_MENU.find('li.active ul').hide();
            $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
        } else {
            $SIDEBAR_MENU.find('li.active-sm ul').show();
            $SIDEBAR_MENU.find('li.active-sm').addClass('active').removeClass('active-sm');
        }

        $BODY.toggleClass('nav-md nav-sm');

        setContentHeight();

        $('.dataTable').each ( function () { $(this).dataTable().fnDraw(); });
    });

    // check active menu
    $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');

    $SIDEBAR_MENU.find('a').filter(function () {
        return this.href === CURRENT_URL;
    }).parent('li').addClass('current-page').parents('ul').slideDown(function() {
        setContentHeight();
    }).parent().addClass('active');

    // recompute content when resizing
    $(window).smartresize(function(){
        setContentHeight();
    });

    setContentHeight();

    // fixed sidebar
    if ($.fn.mCustomScrollbar) {
        $('.menu_fixed').mCustomScrollbar({
            autoHideScrollbar: true,
            theme: 'minimal',
            mouseWheel:{ preventDefault: true }
        });
    }
}

// NProgress
if (typeof NProgress !== 'undefined') {
    $(document).ready(function () {
        NProgress.start();
    });

    $(window).load(function () {
        NProgress.done();
    });
}

//hover and retain popover when on popover content
var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj) {
    var self = obj instanceof this.constructor ?
        obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
    var container, timeout;

    originalLeave.call(this, obj);

    if (obj.currentTarget) {
        container = $(obj.currentTarget).siblings('.popover');
        timeout = self.timeout;
        container.one('mouseenter', function() {
            //We entered the actual popover – call off the dogs
            clearTimeout(timeout);
            //Let's monitor popover content instead
            container.one('mouseleave', function() {
                $.fn.popover.Constructor.prototype.leave.call(self, self);
            });
        });
    }
};

$('body').popover({
    selector: '[data-popover]',
    trigger: 'click hover',
    delay: {
        show: 50,
        hide: 400
    }
});

function init_TagsInput() {
    if (typeof $.fn.tagsInput !== 'undefined') {
        $('#tags_1').tagsInput({
            width: 'auto'
        });
    }
}

function init_select2() {
    if (typeof (select2) === 'undefined') return;

    $(".select2_single").select2({
        placeholder: "Select a state",
        allowClear: true
    });
    $(".select2_group").select2({});
    $(".select2_multiple").select2({
        maximumSelectionLength: 4,
        placeholder: "With Max Selection limit 4",
        allowClear: true
    });
}

function init_InputMask() {
    if (typeof ($.fn.inputmask) === 'undefined') return;

    $(":input").inputmask();
}

function init_daterangepicker_single_call() {
    if( typeof ($.fn.daterangepicker) === 'undefined'){ return; }
    $('#single_cal3').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_3",
        locale: {
            'format': 'DD/MM/YYYY',
            'daysOfWeek': [
                'CN',
                'T2',
                'T3',
                'T4',
                'T5',
                'T6',
                'T7'
            ],
            'monthNames': [
                'Tháng 1',
                'Tháng 2',
                'Tháng 3',
                'Tháng 4',
                'Tháng 5',
                'Tháng 6',
                'Tháng 7',
                'Tháng 8',
                'Tháng 9',
                'Tháng 10',
                'Tháng 11',
                'Tháng 12'
            ]
        }
    }, function(start, end, label) {
        // console.log(start.toISOString(), end.toISOString(), label);
    });
}

/* CALENDAR */
function  init_calendar() {

    if( typeof ($.fn.fullCalendar) === 'undefined'){ return; }

    var date = new Date(),
        d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear(),
        started,
        categoryClass;

    var calendar = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listMonth'
        },
        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
            $('#fc_create').click();

            started = start;
            ended = end;

            $(".antosubmit").on("click", function() {
                var title = $("#title").val();
                if (end) {
                    ended = end;
                }

                categoryClass = $("#event_type").val();

                if (title) {
                    calendar.fullCalendar('renderEvent', {
                            title: title,
                            start: started,
                            end: end,
                            allDay: allDay
                        },
                        true // make the event "stick"
                    );
                }

                $('#title').val('');

                calendar.fullCalendar('unselect');

                $('.antoclose').click();

                return false;
            });
        },
        eventClick: function(calEvent, jsEvent, view) {
            $('#fc_edit').click();
            $('#title2').val(calEvent.title);

            categoryClass = $("#event_type").val();

            $(".antosubmit2").on("click", function() {
                calEvent.title = $("#title2").val();

                calendar.fullCalendar('updateEvent', calEvent);
                $('.antoclose2').click();
            });

            calendar.fullCalendar('unselect');
        },
        editable: true,
        events: [{
            title: 'All Day Event',
            start: new Date(y, m, 1)
        }, {
            title: 'Long Event',
            start: new Date(y, m, d - 5),
            end: new Date(y, m, d - 2)
        }, {
            title: 'Meeting',
            start: new Date(y, m, d, 10, 30),
            allDay: false
        }, {
            title: 'Lunch',
            start: new Date(y, m, d + 14, 12, 0),
            end: new Date(y, m, d, 14, 0),
            allDay: false
        }, {
            title: 'Birthday Party',
            start: new Date(y, m, d + 1, 19, 0),
            end: new Date(y, m, d + 1, 22, 30),
            allDay: false
        }, {
            title: 'Click for Google',
            start: new Date(y, m, 28),
            end: new Date(y, m, 29),
            url: 'http://google.com/'
        }]
    });
}

function init_tinymce() {
    tinymce.init({
        selector: 'textarea#contentEdt',
        height: 300,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'
        ],
        toolbar1: 'undo redo | styleselect | fontsizeselect | bold italic underline | bullist numlist | link image media',
        toolbar2: 'alignleft aligncenter alignright alignjustify |  outdent indent | hr | print preview  | forecolor backcolor emoticons | codesample help',
        image_title: true,
        automatic_uploads: true,
        image_advtab: true,
        file_picker_types: 'image',
        file_picker_callback: function(callback, value, meta) {
            if (meta.filetype === 'image') {
                const $imgContent = $('#imgContent');
                $imgContent.trigger('click');
                $imgContent.on('change', function() {
                    const file = this.files[0];
                    const reader = new FileReader();
                    reader.onload = function(e) {
                        callback(e.target.result, {
                            alt: ''
                        });
                    };
                    reader.readAsDataURL(file);
                });
            }
        },
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
}

function btnConfirmRemove(btnElement, url, type = '') {
    $(btnElement).on('click', function() {
        const name = $(this).data('name');
        const id = $(this).data('id');

        swal({
            title: `Xóa ${type} "${name}"?`,
            text: 'Nội dung bị xóa sẽ không thể khôi phục lại',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Xóa ${type} này`,
            cancelButtonText: `Hủy`
        }).then(function () {
            location.href = `${url}/${id}`;
        }, function (dismiss) {
            if (dismiss === 'cancel') {

            }
        });
    });
}

function btnConfirmChange(btnElement, url) {
    $(btnElement).on('click', function() {
        const id = $(this).data('id');
        swal({
            title: `Yêu cầu đã được xử lý?`,
            text: 'Tác vụ sẽ không được hoàn tác',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: `Xác nhận`,
            cancelButtonText: `Hủy`
        }).then(function () {
            location.href = `${url}/${id}`;
        }, function (dismiss) {
            if (dismiss === 'cancel') {

            }
        });
    });
}

function init_modal_request() {
    $('.btn-modal-request').click(function () {
        const $id = $(this).data('id');
        const $cName = $(this).data('name');
        const $cNationality = $(this).data('nationality');
        const $cPhone = $(this).data('phone');
        const $cEmail = $(this).data('email');
        const $type = $(this).data('type');
        const $price = $(this).data('price');
        const $checkIn = $(this).data('checkIn');
        const $city = $(this).data('city');
        const $district = $(this).data('district');
        const $desc = $(this).data('desc');
        const $status = $(this).data('status');

        const $btnModalRA = $('.btn-modal-request-accept');

        if (parseInt($status) === 2) {
            $btnModalRA.attr('disabled', true).hide();
        }

        if ($(this).data('images')) {
            const $images = $(this).data('images');
            $images.split(',').forEach(pic => {
                $('.request-images').append(
                    `<a href="${pic}" target="_blank"><img src="${pic}" width="70px" height="70px" style="margin-right: 5px"></a>`
                );
            });
        }

        $btnModalRA.data('name', $cName);
        $btnModalRA.data('id', $id);

        $('.modal-request-name').text($cName);
        $('.modal-request-nationality').text($cNationality);
        $('.modal-request-email').text($cEmail);
        $('.modal-request-phone').text($cPhone);
        $('.modal-request-type').text($type);
        $('.modal-request-price').text($price);
        $('.modal-request-checkIn').text($checkIn);
        $('.modal-request-city').text($city);
        $('.modal-request-district').text($district);
        $('.modal-request-desc').text($desc);
    });
}

function init_dynamic_form() {
    const $area = $('#area');
    const $districts = $('.districts');
    $districts.attr('disabled', 'true').removeAttr('required').hide();
    if ($area.val() === '') {
        $('.districts-active').removeAttr('disabled').attr('required', 'true').show();
    } else {
        $districts.each(function () {
            const $district = $(this);
            if ($district.prop('id') === $area.val()) {
                $district.removeAttr('disabled').attr('required', 'true').show();
            }
        });
    }

    $area.on('change', function () {
        const areaId = $(this).val();
        $districts.attr('disabled', 'true').removeAttr('required').hide();

        $districts.each(function () {
            const $district = $(this);
            if ($district.prop('id') === areaId) {
                $district.removeAttr('disabled').attr('required', 'true').show();
            }
        });
    });
}

function init_format_price() {
    function formatMoney(money) {
        let total;
        let a;
        if (money > 1e9) {
            a = money / 1e9;
            if (money % 1e9 === 0) {
                total = a + ' tỷ';
            } else {
                total = Math.floor(a) + ' tỷ ' + (money % 1e9) / 1e6;
            }
        } else if (money > 1e6) {
            a = money / 1e6;
            if (money % 1e6 === 0) {
                total = a + ' triệu';
            } else {
                total = Math.floor(a) + ' triệu ' + (money % 1e6) / 1e3;
            }
        } else if (money > 1e3) {
            a = money / 1e3;
            if (money % 1e3 === 0) {
                total = a + ' nghìn đồng';
            } else {
                total = Math.floor(a) + ' nghìn ' + (money % 1e3);
            }
        }

        return total;
    }

    let priceType = $('#priceType').val();

    $('#price').keyup(function () {
        const $price = $(this).val() * 1000;
        if ($price === 0) {
            $('#textPriceHidden').val('');
            $('#textPrice').text('');
            $('#realPriceHidden').val(0);
        } else {
            $('#realPriceHidden').val($price);
            $('#textPrice').text(formatMoney($price));
            $('#textPriceHidden').val(formatMoney($price) + priceType);
            $('#priceType').change(function () {
                priceType = $(this).val();
                $('#textPriceType').text($(this).val());
                $('#textPriceHidden').val(formatMoney($price) + priceType);
            });
        }
    });
}

function init_format_url() {
    $('#titleURL').keyup(function () {
        const title = $(this).val();
        if (title === '') {
            $('#friendlyURL').val('');
        } else {
            const token = Math.random().toString(36).substr(9);
            $('#friendlyURL').val(getSlug(title, {lang: 'vn'}) + '-' + token);
        }
    });
}

function init_parsley() {
    const $formParsley = $('.form-validation-pl');

    if ($formParsley.parsley()) {
        $formParsley.parsley().on('form:submit', function (e) {
            $('button[type=submit]').attr('disabled', 'disabled');
        });
    } else {
        $('form').submit(function () {
            $('button[type=submit]').attr('disabled', 'disabled');
        });
    }
}

function init_selectPropertyDistrict() {
    const $selectDirection = $('#direction');
    const dataDirection = $selectDirection.data('direction');
    $selectDirection.children().each(function (i, e) {
        const optionDirection = $(this);
        if (optionDirection.attr('value') === dataDirection) {
             optionDirection.attr('selected', true);
        }
    })
}

function init_select2_flags() {
    function formatSelectFlags (state) {
        if (!state.id) return state.text;
        const flag = $(state.element).data('flag') || state.element.value;
        return $(`<span><img src="/public/flags/${flag.toLowerCase()}.png" class="img-flag"/> ${state.text}</span>`);
    }

    $('.select2.flags').each(function () {
        $(this).select2({
            templateResult: formatSelectFlags,
            templateSelection: formatSelectFlags
        });
    });
}

function init_select_current_flag() {
    const $flag = $('#flag');
    const currentFlag = $flag.data('flag');
    $flag.children('option').each(function () {
        if ($(this).val() === currentFlag) {
            $(this).attr('selected', true);
        }
    });
}

function init_editUrl() {
	const $btnEdit = $('#btnEditUrl');
	$btnEdit.click(function () {
		$('input[name=url]').removeAttr('readonly');
	});
}

function init_switch() {
	const checkSwitch = $('.js-switch');
	if (checkSwitch) {
		const that = $(this);
		const elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
		elems.forEach(html => {
			new Switchery(html, {
				color: '#26B99A'
			});
		});

		$(this).change(function () {
			if (checkSwitch.is(":checked")) {
				that.val('1');
			} else {
				that.val('2');
			}
		})
	}
}

function genIdProduct() {
	$('.categoryID').change(function () {
		const $currentSelect = $('.categoryID :selected');

		if ($currentSelect.val() !== '') {
			const token = Math.random().toString().substr(13);

			const optText = $currentSelect.text().toUpperCase();
			let textShort = optText.charAt(0);

			for (let i = 0; i < optText.length; i++) {
				if (optText.charAt(i) === ' ' && optText.charAt(i + 1) !== '/') {
					textShort += optText.charAt(i + 1);
				}
			}

			$('#productCode').val(textShort + token);
		} else {
			$('#productCode').val('');
		}
	});
}

function handleSubscribe() {
	$('.btn-handle-subscribe').on('click', function () {
		swal({
			title: 'Are you sure?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Kê~!',
			cancelButtonText: 'Hủy',
		}).then(() => {
			const id = $(this).data('id');
			$.ajax({
				url: '/admin/handle-subscribe',
				type: 'POST',
				data: { id },
				dataType: 'text',
				success: (result) => {
					if (result === 'true') {
						$(`#${id}`).html('<span class="label label-success label-lg">Đã xử lý</span>');
					} else {
						$(`#${id}`).html('<span class="label label-warning label-lg">Chưa xử lý</span>');
					}
					swal({
						type: 'success',
						title: 'Your work has been saved',
						showConfirmButton: false,
						timer: 1500
					});
				}
			});
		})
	});
}

function deleteSubscribe() {
	$('.btn-delete-subscribe').on('click', function () {
		swal({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete it!'
		}).then(() => {
			const id = $(this).data('id');
			$.ajax({
				url: '/admin/delete-subscribe',
				type: 'POST',
				data: { id },
				dataType: 'text',
				success: (result) => {
					if (result) {
						$(`#wrap-${id}`).addClass('bg-red');
						$(`#wrapBtn-${id}`).hide();
						swal({
							type: 'success',
							title: 'Your work has been saved',
							showConfirmButton: false,
							timer: 1500
						});
					}
				}
			});
		})
	});
}

function truncateText(maxLength = 50) {
	let text = $('.truncateText').text();
	if (text.length > maxLength) text = `${text.substr(0, maxLength)}...`;

	return text;
}

$(document).ready(function() {
    init_sidebar();
    init_InputMask();
    init_TagsInput();
    init_daterangepicker_single_call();
	genIdProduct();
	handleSubscribe();
	deleteSubscribe();

    init_select2();
    init_calendar();
    init_tinymce();
    init_modal_request();
    init_dynamic_form();
    init_format_price();
    init_format_url();
    init_parsley();
    init_selectPropertyDistrict();
    init_select2_flags();
    init_select_current_flag();
	init_editUrl();
	init_switch();

    btnConfirmRemove('.btn-delete-category', '/admin/blog/category/remove', 'chuyên mục');
    btnConfirmRemove('.btn-delete-blog-post', '/admin/blog/your-posts/remove', 'bài đăng');
    btnConfirmRemove('.btn-delete-category-property', '/admin/property/category/remove','chuyên mục');
    btnConfirmRemove('.btn-delete-property-post', '/admin/property/posts-manager/remove', 'bài đăng');
    btnConfirmRemove('.btn-delete-area', '/admin/area/delete', 'khu vực');
    btnConfirmRemove('.btn-delete-district', '/admin/district/delete', 'quận');
	btnConfirmRemove('.btn-delete-language', '/admin/language/delete', 'ngôn ngữ');

    btnConfirmChange('.btn-modal-request-accept-find', '/admin/request/find-property/handling');
    btnConfirmChange('.btn-modal-request-accept-post', '/admin/request/post-property/handling');
    btnConfirmChange('.btn-request-report-change', '/admin/request/report/handling');

    $('#btnSubmit').click(function () {
        $('#imgContent').prop('disabled', 'disabled');
    });
});
