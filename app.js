const express = require('express');
const session = require('express-session');
const expressValidator = require('express-validator');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);
const flash = require('connect-flash');
const passport = require('passport');
const compression = require('compression');
const ejsMate = require('ejs-mate');
const favicon = require('serve-favicon');
const path = require('path');
const helmet = require('helmet');

const routes = require('./routes');
const secret = require('./services/secret');
const sitemap = require('./services/sitemap.config');
const minifyHTML = require('./services/minifyHTML.config');

mongoose.Promise = global.Promise;
mongoose.connect(secret.database, { config: { autoIndex: false }});

const app = express();

app.use(helmet());
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.enable('trust proxy');
app.use(compression());
app.use(minifyHTML);
app.use('/public', express.static('./public',  {
	// maxage: '8760h'
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({
	secret: secret.secretKey,
	name: 'cityavn.cookie',
	resave: false,
	saveUninitialized: false,
	cookie: {
		HttpOnly: true,
		secure: false,
		maxAge: 10000 * 60 * 60 // 15 phút
	},
	store: new MongoStore({url: secret.database})
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(expressValidator({
	errorFormatter: (param, msg, value) => {
		let namespace = param.split('.')
			, root    = namespace.shift()
			, formParam = root;

		while (namespace.length) {
			formParam += '[' + namespace.shift() + ']';
		}
		return {
			param : formParam,
			msg,
			value
		};
	}
}));

app.use(['/admin', '/auth'], (req, res, next) => {
	res.locals.user = req.user;
	next();
});

app.use(['/admin', '/auth'], (req, res, next) => {
	res.locals.flash_messages = req.session.flash;
	delete req.session.flash;
	next();
});

app.get('/sitemap.xml', function(req, res) {
	sitemap.toXML( function (err, xml) {
		if (err) return res.status(500).end();

		res.header('Content-Type', 'application/xml');
		res.send(xml);
	});
});

app.engine('ejs', ejsMate);
app.set('view engine', 'ejs');
app.set('views', './views');

app.use(routes);

app.listen(process.env.PORT || secret.port);
