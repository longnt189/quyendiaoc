const Administrators = require('../models/Administrators');
const Register = require('../models/Register');

const { signUpValidatorConfig } = require('../services/expressValidator.config');
const { checkPagination } = require('../helpers/checkPagination');
const getNumberOrder = require('../helpers/getNumberOrder');

const perPage = 30;

module.exports = {
	signUp: async (req, res, next) => {
		try {
			const validationError = signUpValidatorConfig(req);

			if (validationError) {
				validationError.forEach(e => {
					req.flash('error', e.msg );
				});
				return  res.redirect('/admin/create-account');
			}

			const { email, password, name } = req.body;

			const isExistAd = await Administrators.findOne({ email });
			if (isExistAd) {
				req.flash('error', 'Tên đăng nhập đã tồn tại!');
				return res.redirect('/admin/create-account');
			}

			const administrator = new Administrators();
			administrator.profile.name = name;
			administrator.email = email;
			administrator.password = password;
			administrator.profile.picture = administrator.gravatar();
			await administrator.save();

			req.flash('success', 'Tạo tài khoản thành công!');
			res.redirect('/admin/create-account');
		} catch (err) {
			next(err);
		}
	},
	changePassword: async (req, res, next) => {
		try {
			const { id, currentPassword, newPassword } = req.body;
			const currentAd = await Administrators.findById(id);
			if (currentAd && currentAd.comaprePassword(currentPassword)) {
				currentAd.password = newPassword;
				await currentAd.save();

				req.flash('success', 'Đổi mật khẩu thành công!');
				res.redirect('/admin/profile');
			}
		} catch (err) {
			next(err.toString());
		}
	},
	subscribe: async (req, res, next) => {
		try {
			const { fullName, phone, email, content } = req.body;

			const register = new Register({
				fullName,
				phone
			});

			if (email) register.email = email;
			if (content) register.content = content;

			await register.save();

			res.redirect('/thank-you');
		} catch (err) {
			next(err);
		}
	},
	getSubscribers: async (req, res, next) => {
		try {
			const currentPage = checkPagination(req);
			const numberOrder = getNumberOrder(currentPage, perPage);


			const { handled } = req.query;

			let query = {};

			if (handled === 'true') {
				query = { isHandled: handled === 'true', deleteAt: { $exists: false } };
			} else if (handled === 'false') {
				query = { isHandled: handled === 'true', deleteAt: { $exists: false } };
			}

			const total = await Register.count(query);
			const countPages = Math.ceil(total / perPage);

			let queryUrl = '';
			for (let e in req.query) {
				if (e && e !== 'page') queryUrl += `${e}=${req.query[e]}&`;
			}

			const registers = await Register
				.find(query)
				.select('-updatedAt -__v')
				.sort({ createdAt: -1 })
				.skip(perPage * (currentPage - 1))
				.limit(perPage);

			res.render('admin/main/subcribers', {
				registers,
				currentPage,
				countPages,
				numberOrder,
				queryUrl
			});
		} catch (err) {
			next(err);
		}
	},
	handleSubscribe: async (req, res, next) => {
		try {
			const { id } = req.body;
			const subscriber = await Register.findById(id).select('isHandled');
			if (!subscriber) throw new Error('Id không tồn tại');

			subscriber.isHandled = !subscriber.isHandled;

			const result = await subscriber.save();
			res.send(result.isHandled);
		} catch (err) {
			next(err);
		}
	},
	deleteSubscribe: async (req, res, next) => {
		try {
			const { id } = req.body;
			const subscriber = await Register.findById(id).select('isHandled');
			if (!subscriber) throw new Error('Id không tồn tại');

			subscriber.deleteAt = Date.now();
			const result = await subscriber.save();

			res.send(result.deleteAt);
		} catch (err) {
			next(err);
		}
	}
};
